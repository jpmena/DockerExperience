# About Using Markdown for documentations

## [Atom Code snippets](https://github.com/atom/language-gfm/blob/master/snippets/gfm.cson)
* An insteresting link for ATOM Snipped when Markdown-In
  * https://github.com/atom/language-gfm/blob/master/snippets/gfm.cson
* I could add the [ATOM GFM package](https://github.com/atom/language-gfm)
* There is nothing to do, this package belongs to the core:

```bash
[jpmena@localhost DockerExperience]$ apm install language-gfm
The language-gfm package is bundled with Atom and should not be explicitly installed.
You can run `apm uninstall language-gfm` to uninstall it and then the version bundled
with Atom will be used.
Installing language-gfm to /home/jpmena/.atom/packages ✓
[jpmena@localhost DockerExperience]$ apm uninstall language-gfm
Uninstalling language-gfm ✓

```

* If I create my snippet :

```
[jpmena@localhost .atom]$ diff -u snippets.10022018.cson snippets.cson
--- snippets.10022018.cson	2018-02-04 18:20:01.568997165 +0100
+++ snippets.cson	2018-02-10 18:20:53.607914224 +0100
@@ -19,3 +19,7 @@
 # If you are unfamiliar with CSON, you can read more about it in the
 # Atom Flight Manual:
 # http://flight-manual.atom.io/using-atom/sections/basic-customization/#_cson
+'.source.gfm':
+   'Code Source':
+     'prefix': 'cs'
+     'body': '```'
```

* the project proposes for such a snippet ([coffee script](http://coffeescript.org/) format)

```coffee
'code':
    'prefix': 'code'
    'body': """
      ```$1
      $2
      ```$0
    """
```

## [Visual Studio Code Snippets](https://code.visualstudio.com/docs/editor/userdefinedsnippets)

* There are already [snippets for Markdown](https://code.visualstudio.com/docs/languages/markdown#_snippets-for-markdown) in the Visual Code Store
  * **CTRL+Space** to acivate suggestions, then type the first letters or follow the list wwith the arraows, and when found just press **TAB**

### [Working with MD](https://code.visualstudio.com/docs/languages/markdown)
* [Synchronizaion](https://code.visualstudio.com/docs/languages/markdown#_editor-and-preview-synchronization) is a great feature !!!

### [Visual Studio ShortCuts For MarkDown](https://marketplace.visualstudio.com/items?itemName=mdickin.markdown-shortcuts)


* There is [an extension](https://marketplace.visualstudio.com/items?itemName=mdickin.markdown-shortcuts) for that.
  * it could help me access the terminal without having to use menu/submenu

```bash
[jpmena@localhost ~]$ code --install-extension mdickin.markdown-shortcuts
Found 'mdickin.markdown-shortcuts' in the marketplace.
Installing...
Extension 'mdickin.markdown-shortcuts' v0.8.1 was successfully installed!

```

## Main [Visual Studio Code ShortCuts](https://code.visualstudio.com/docs/getstarted/keybindings)

* You access the Configurable Shortcuts page through : **Fichier / Preferences / Raccourcis Clavier**

* To access the terminal using **CTRL+T**
  * search for *ouvrir terminal intégré* on that page
  * clik the edit pen on the left
  * Type *CTRL* and, withhout releasig the previous key, type *T*




## I want a [Linux terminal on ATOM](http://atom-packages.directory/category/tools-terminal/)

* A often downloaded project [terminal plus](https://atom.io/packages/terminal-plus)

```bash
jpmena@localhost .atom]$ apm install terminal-plus
Installing terminal-plus to /home/jpmena/.atom/packages
```

* You have before to install [node-gyp through npm](https://github.com/nodejs/node-gyp#installation)
  * that means [install manually node](https://nodejs.org/en/download/)!
    * in that case download [the linux 64Bits binaries](https://nodejs.org/dist/v8.9.4/node-v8.9.4-linux-x64.tar.xz)
    * And add the binaries directory to the user path !!!

### Manually installing [Node and npm](https://nodejs.org/en/download/current/)

```bash
# untar the binaries
[jpmena@localhost CONSULTANT]$ tar xf node-v8.9.4-linux-x64.tar.xz -C ~
[jpmena@localhost CONSULTANT]$ ll ~/node-v8.9.4-linux-x64/bin/
total 34376
-rwxrwxr-x. 1 jpmena jpmena 35200855  3 janv. 03:23 node
lrwxrwxrwx. 1 jpmena jpmena       38  3 janv. 03:23 npm -> ../lib/node_modules/npm/bin/npm-cli.js
lrwxrwxrwx. 1 jpmena jpmena       38  3 janv. 03:23 npx -> ../lib/node_modules/npm/bin/npx-cli.js
#add the bin directory to the PATH
## save the previous user configuration
[jpmena@localhost ~]$ cp -pv .bash_profile .bash_profile_bak$(date '+%d%m%Y')
'.bash_profile' -> '.bash_profile_bak10022018'
##after editing we should have somthing like the following diff result:
[jpmena@localhost ~]$ diff -u .bash_profile .bash_profile_bak$(date '+%d%m%Y')
--- .bash_profile	2018-02-10 18:57:13.624060017 +0100
+++ .bash_profile_bak10022018	2017-10-30 15:05:00.000000000 +0100
@@ -6,7 +6,7 @@
 fi

 # User specific environment and startup programs
-NODE_HOME=$HOME/node-v8.9.4-linux-x64
-PATH=$PATH:$NODE_HOME/bin:$HOME/.local/bin:$HOME/bin
+
+PATH=$PATH:$HOME/.local/bin:$HOME/bin

 export PATH

```

* we restart the session and test the versions:

```bash
#we load the new configuration
[jpmena@localhost ~]$ source .bash_profile
#which versions do we have ?
[jpmena@localhost ~]$ node --version
v8.9.4
[jpmena@localhost ~]$ npm --version
5.6.0
```

### installing [node_gyp](https://github.com/nodejs/node-gyp)

```bash
[jpmena@localhost ~]$ npm install -g node-gyp
/home/jpmena/node-v8.9.4-linux-x64/bin/node-gyp -> /home/jpmena/node-v8.9.4-linux-x64/lib/node_modules/node-gyp/bin/node-gyp.js
+ node-gyp@3.6.2
added 101 packages in 5.493s
```

### Terminal-plus does not work !!!!
* sur ce [forum](https://github.com/jeremyramin/terminal-plus/issues/280) on nous explique que terminal-plus n'est plus maintenu !!!
* On fait un retour arrière
```bash
[jpmena@localhost ~]$ apm uninstall terminal-plus
Uninstalling terminal-plus ✓
[jpmena@localhost ~]$ npm uninstall -g node-gyp
removed 101 packages in 3.65s
```

### In the prevoius forum they suggest [platformio-ide-terminal](https://atom.io/packages/platformio-ide-terminal)

```bash
[jpmena@localhost ~]$ apm install platformio-ide-terminal
Installing platformio-ide-terminal to /home/jpmena/.atom/packages ✓
```
## The previous one did not quite work, so I try [atom-terminal](

```bash
  [jpmena@localhost ~]$ apm install atom-terminal
  Installing atom-terminal to /home/jpmena/.atom/packages ✓
  [jpmena@localhost ~]$ apm uninstall atom-terminal
  Uninstalling atom-terminal ✓
```
