# Chapter 3: Installing docker

* p 25: the client can be on any platform
  * But the community has built an entire [toolbow for Windows](https://docs.docker.com/toolbox/toolbox_install_windows/)
* The server has to be on a host running the Linux Kernel ::::
> Although the Docker client can run on Windows and Mac OS X to control a Docker
Server, Docker containers can only be built and launched on Linux. Therefore, non-
Linux systems will require a virtual machine or remote server to host the Linux-based
Docker server.

* p 26 interesting definitions to come back to !!!!
  * Image: filesystem layers !!!
> To develop with Docker on non-Linux platforms, you will need to
leverage virtual machines or remote Linux hosts to provide a
Docker server. Docker Machine, Boot2Docker, and Vagrant, which
are discussed later in this chapter, provide some approaches to
solving this issue.

* p 28 we still deal with Fedora 21 (Book written in 2015). The installation uses [the script](https://get.docker.com/)
  * that script has since been extended to Ubuntu distributions !!!
  * Though for Ubuntu distributions at that time ... the book uses key + apt sources
* The warning is important
> It is never a great idea to run a script from the Internet and pipe it
into a shell. Even if it comes from a trusted source, sites get com‐
promised frequently. If you are going to use this method, at least
download the script and read it before piping it into a shell.

* So here is my [local version of the script to be read before use](../../shells/getDocckerLinux.sh)


* the book also propose a manual Fedor21 installation ...
* The script proposal

```bash
centos|fedora)
  yum_repo="$DOWNLOAD_URL/linux/$lsb_dist/docker-ce.repo"
  if [ "$lsb_dist" = "fedora" ]; then
    if [ "$dist_version" = "24" ]; then
      echo
      echo "Warning: Fedora 24 has reached EOL"
      echo "         Support for Fedora 24 for this installation script will be removed on October 1, 2017"
      echo
      sleep 10
    fi
    if [ "$dist_version" -lt "24" ]; then
      echo "Error: Only Fedora >=24 are supported"
      exit 1
    fi
    pkg_manager="dnf"
    config_manager="dnf config-manager"
    enable_channel_flag="--set-enabled"
    pre_reqs="dnf-plugins-core"
  else
    pkg_manager="yum"
    config_manager="yum-config-manager"
    enable_channel_flag="--enable"
    pre_reqs="yum-utils"
  fi
  (
    if ! is_dry_run; then
      set -x
    fi
    $sh_c "$pkg_manager install -y -q $pre_reqs"
    $sh_c "$config_manager --add-repo $yum_repo"

    if [ "$CHANNEL" != "stable" ]; then
      $sh_c "$config_manager $enable_channel_flag docker-ce-$CHANNEL"
    fi
    $sh_c "$pkg_manager makecache"
    $sh_c "$pkg_manager install -y -q docker-ce"
  )
```

## For Windows and OSX

* those are non Linux containers ...
* so p 30 !!!
>You will also need to download and install VirtualBox, which Mac OS X requires to
launch Linux virtual machines that can build Docker images and run containers.


## Docker server

* Same binary between server and client ...
* On ne démarre plus docker de la même façon

```bash  
[jpmena@localhost ~]$ ps -ef | grep -i docker
root      1229     1  0 11:51 ?        00:00:27 /usr/bin/dockerd
root      1299  1229  0 11:51 ?        00:00:21 docker-containerd --config /var/run/docker/containerd/containerd.toml
```

* p 32: interesting systemd command (generally interesting) ....
* for the page 33/34 I am on a local OSX terminal !!!!
  * the client itself stays on the OSX
  * Is is the docker server, the host that is here names __docker-mahine__
  * to access the machine, I have to add the machine/server env variables to docker using the eval shell in blod: _docker __$(docker-machine config local)__ ps_
* page 35: we can easaly log into the machine !!!!

* p 39: [Vagrant](https://www.vagrantup.com/): suport for multiple Hypervisors
  * [VirtualBox](https://www.virtualbox.org/wiki/Downloads) is such a hypervisor !!!

## Installing with [vagrant](https://www.vagrantup.com/)) (p 39!!!)

* [Downloading Vagrant](https://www.vagrantup.com/downloads.html) like explained on page 39!!
  * I could take the [CentOS7 64Bits Version](https://releases.hashicorp.com/vagrant/2.0.2/vagrant_2.0.2_x86_64.rpm?_ga=2.32487484.383263465.1518340724-464138673.1518340724)
  * But for my Fedora Computer I prefer [that Fedora Vagrant Link](https://developer.fedoraproject.org/tools/vagrant/vagrant-virtualbox.html)

```bash
[jpmena@localhost ~]$ sudo dnf install vagrant
[sudo] Mot de passe de jpmena : 
Dernière vérification de l’expiration des métadonnées effectuée il y a 0:00:00 le dim. 11 févr. 2018 10:51:16 CET.
................................................
Installer  57 Paquets

Taille totale des téléchargements : 14 M
Taille des paquets installés : 46 M
Voulez-vous continuer ? [o/N] :
................................

Installé :
  vagrant.noarch 2.0.1-1.fc27                            libguestfs-tools-c.x86_64 1:1.37.35-1.fc27                libguestfs-xfs.x86_64 1:1.37.35-1.fc27          rubygem-bigdecimal.x86_64 1.3.0-86.fc27         
  rubygem-did_you_mean.x86_64 1.1.0-86.fc27              rubygem-rdoc.noarch 5.1.0-2.fc27                          vagrant-libvirt.noarch 0.0.40-3.fc27            bsdtar.x86_64 3.3.1-3.fc27                      
  compat-readline6.x86_64 6.3-13.fc27                    hexedit.x86_64 1.2.13-11.fc27                             hivex.x86_64 1.3.14-8.fc27                      libguestfs.x86_64 1:1.37.35-1.fc27              
  libldm.x86_64 0.2.3-9.fc24                             libyaml.x86_64 0.1.7-4.fc27                               lsscsi.x86_64 0.28-6.fc27                       ruby.x86_64 2.4.3-86.fc27                       
  ruby-irb.noarch 2.4.3-86.fc27                          ruby-libs.x86_64 2.4.3-86.fc27                            rubygem-builder.noarch 3.2.3-3.fc27             rubygem-childprocess.noarch 0.5.9-3.fc27        
  rubygem-domain_name.noarch 0.5.20170404-2.fc27         rubygem-erubis.noarch 2.7.0-14.fc27                       rubygem-excon.noarch 0.54.0-3.fc27              rubygem-ffi.x86_64 1.9.18-3.fc27                
  rubygem-fog-core.noarch 1.43.0-3.fc27                  rubygem-fog-json.noarch 1.0.2-4.fc27                      rubygem-fog-libvirt.noarch 0.3.0-3.fc27         rubygem-fog-xml.noarch 0.1.2-4.fc27             
  rubygem-formatador.noarch 0.2.5-4.fc27                 rubygem-hashicorp-checkpoint.noarch 0.1.4-5.fc27          rubygem-http-cookie.noarch 1.0.3-3.fc27         rubygem-i18n.noarch 0.7.0-5.fc27                
  rubygem-io-console.x86_64 0.4.6-86.fc27                rubygem-json.x86_64 2.1.0-102.fc27                        rubygem-listen.noarch 3.1.5-3.fc27              rubygem-log4r.noarch 1.1.10-6.fc27              
  rubygem-mime-types.noarch 3.1-3.fc27                   rubygem-mime-types-data.noarch 3.2016.0521-3.fc27         rubygem-multi_json.noarch 1.12.1-3.fc27         rubygem-net-scp.noarch 1.2.1-7.fc27             
  rubygem-net-sftp.noarch 2.1.2-6.fc27                   rubygem-net-ssh.noarch 4.1.0-2.fc27                       rubygem-netrc.noarch 0.11.0-2.fc27              rubygem-nokogiri.x86_64 1.8.1-1.fc27            
  rubygem-openssl.x86_64 2.0.5-86.fc27                   rubygem-psych.x86_64 2.2.2-86.fc27                        rubygem-rb-inotify.noarch 0.9.7-4.fc27          rubygem-rest-client.noarch 2.0.0-3.fc27         
  rubygem-ruby-libvirt.x86_64 0.7.0-6.fc27               rubygem-unf.noarch 0.1.4-9.fc27                           rubygem-unf_ext.x86_64 0.0.7.4-3.fc27           rubygems.noarch 2.6.14-86.fc27                  
  rubypick.noarch 1.1.1-7.fc27                           scrub.x86_64 2.5.2-11.fc27                                squashfs-tools.x86_64 4.3-15.fc27               supermin.x86_64 5.1.19-1.fc27                   
  zerofree.x86_64 1.0.3-9.fc27                          

Terminé !

  ```

  * Note that it installs Ruby and all the necessary dependencies !!!
* Working with a centOS Server hosing Core OS based images !

# p 41 Start a vagrant Host 

* can only be started where a cconfig.rb is present

```bash
# Where am I ?
[jpmena@localhost coreos-vagrant]$ pwd
/home/jpmena/CONSULTANT/docker-host/coreos-vagrant
# Starting the Vagrant Virtual Machine !!!!
[jpmena@localhost coreos-vagrant]$ vagrant up
Installing plugins: vagrant-ignition
Installing the 'vagrant-ignition' plugin. This can take a few minutes...
/usr/share/gems/gems/psych-2.2.2/lib/psych.rb:228: warning: already initialized constant Psych::LIBYAML_VERSION
/usr/share/ruby/psych.rb:228: warning: previous definition of LIBYAML_VERSION was here
/usr/share/gems/gems/psych-2.2.2/lib/psych.rb:230: warning: already initialized constant Psych::FALLBACK
/usr/share/ruby/psych.rb:230: warning: previous definition of FALLBACK was here
Fetching: vagrant-libvirt-0.0.43.gem (100%)
Fetching: vagrant-ignition-0.0.3.gem (100%)
Installed the plugin 'vagrant-ignition (0.0.3)'!
Bringing machine 'core-01' up with 'virtualbox' provider...
==> core-01: Box 'coreos-alpha' could not be found. Attempting to find and install...
    core-01: Box Provider: virtualbox
    core-01: Box Version: >= 0
==> core-01: Loading metadata for box 'https://alpha.release.core-os.net/amd64-usr/current/coreos_production_vagrant_virtualbox.json'
    core-01: URL: https://alpha.release.core-os.net/amd64-usr/current/coreos_production_vagrant_virtualbox.json
==> core-01: Adding box 'coreos-alpha' (v1688.0.0) for provider: virtualbox
    core-01: Downloading: https://alpha.release.core-os.net/amd64-usr/1688.0.0/coreos_production_vagrant_virtualbox.box
    core-01: Calculating and comparing box checksum...
==> core-01: Successfully added box 'coreos-alpha' (v1688.0.0) for 'virtualbox'!
==> core-01: Importing base box 'coreos-alpha'...
==> core-01: Configuring Ignition Config Drive
==> core-01: Matching MAC address for NAT networking...
==> core-01: Checking if box 'coreos-alpha' is up to date...
==> core-01: Setting the name of the VM: coreos-vagrant_core-01_1518884922615_39817
==> core-01: Clearing any previously set network interfaces...
==> core-01: Preparing network interfaces based on configuration...
    core-01: Adapter 1: nat
    core-01: Adapter 2: hostonly
==> core-01: Forwarding ports...
    core-01: 2375 (guest) => 2375 (host) (adapter 1)
    core-01: 22 (guest) => 2222 (host) (adapter 1)
==> core-01: Running 'pre-boot' VM customizations...
==> core-01: Booting VM...
==> core-01: Waiting for machine to boot. This may take a few minutes...
    core-01: SSH address: 127.0.0.1:2222
    core-01: SSH username: core
    core-01: SSH auth method: private key
==> core-01: Machine booted and ready!
==> core-01: Setting hostname...
==> core-01: Configuring and enabling network interfaces...
==> core-01: Running provisioner: file...
==> core-01: Running provisioner: shell...
    core-01: Running: inline script
==> core-01: Running provisioner: shell...
    core-01: Running: inline script
# accessing the vagrant Host
[jpmena@localhost coreos-vagrant]$ vagrant ssh
Last login: Sat Feb 17 16:41:33 UTC 2018 from 10.0.2.2 on pts/0
Container Linux by CoreOS alpha (1688.0.0)
core@core-01 ~ $ ls -al
total 44
drwxr-xr-x. 3 core core 4096 Feb 17 16:42 .
drwxr-xr-x. 3 root root 4096 Feb 14 04:31 ..
-rw-------. 1 core core    0 Feb 17 16:28 .authorized_keys.d.lock
-rw-------. 1 core core   15 Feb 17 16:42 .bash_history
lrwxrwxrwx. 1 core core   33 Feb 14 04:31 .bash_logout -> ../../usr/share/skel/.bash_logout
lrwxrwxrwx. 1 core core   34 Feb 14 04:31 .bash_profile -> ../../usr/share/skel/.bash_profile
lrwxrwxrwx. 1 core core   28 Feb 14 04:31 .bashrc -> ../../usr/share/skel/.bashrc
drwx------. 3 core core 4096 Feb 17 16:28 .ssh
## typing exit to quit the vagrant host !!!
Connection to 127.0.0.1 closed.
# don't forget to ne in the right directory to manage your vagrant Docker Host
[jpmena@localhost coreos-vagrant]$ pwd
/home/jpmena/CONSULTANT/docker-host/coreos-vagrant
# Where has my Vagrant Docker Host been downloaded ?
## there is a .vagrant.d directory at my $HOME
[jpmena@localhost .vagrant.d]$ pwd
/home/jpmena/.vagrant.d
## and in its boxes subdirectory you find my boxes !!!!
[jpmena@localhost .vagrant.d]$ cd boxes/
[jpmena@localhost boxes]$ ll
total 4
drwxrwxr-x. 3 jpmena jpmena 4096 Feb 17 17:28 coreos-alpha
[jpmena@localhost boxes]$ cd coreos-alpha/
[jpmena@localhost coreos-alpha]$ ll
total 8
drwxrwxr-x. 3 jpmena jpmena 4096 Feb 17 17:28 1688.0.0
-rw-rw-r--. 1 jpmena jpmena   93 Feb 17 17:28 metadata_url
## note the metadata_url !!!
[jpmena@localhost coreos-alpha]$ cat metadata_url
https://alpha.release.core-os.net/amd64-usr/current/coreos_production_vagrant_virtualbox.json
[jpmena@localhost coreos-alpha]$ cd 1688.0.0/
[jpmena@localhost 1688.0.0]$ ll
total 4
drwxrwxr-x. 2 jpmena jpmena 4096 Feb 17 17:28 virtualbox
[jpmena@localhost 1688.0.0]$ cd virtualbox/
[jpmena@localhost virtualbox]$ ll
total 876452
-rw-r--r--. 1 jpmena jpmena        77 Feb 17 17:28 base_mac.rb
-rw-r--r--. 1 jpmena jpmena     10490 Feb 17 17:28 box.ovf
-rw-r--r--. 1 jpmena jpmena       435 Feb 17 17:28 change_host_name.rb
-rw-r--r--. 1 jpmena jpmena       448 Feb 17 17:28 configure_networks.rb
-rw-r--r--. 1 jpmena jpmena 897449984 Feb 17 17:28 coreos_production_vagrant_virtualbox_image.vmdk
-rw-r--r--. 1 jpmena jpmena        27 Feb 17 17:28 metadata.json
-rw-r--r--. 1 jpmena jpmena       944 Feb 17 17:28 Vagrantfile
```

* In order for Docker to access Containers managed by the server running on that Vagrant Host, one environment variable setting (on my host) is enough:
  * see p 41

```bash
#only variable needs too be set
[jpmena@localhost docker-host]$ export DOCKER_HOST=tcp://127.0.0.1:2375
[jpmena@localhost docker-host]$ env | grep -i docker
PWD=/home/jpmena/CONSULTANT/docker-host
DOCKER_HOST=tcp://127.0.0.1:2375

```