
# Chapter 2 of [Docker Up And Running OReilly](http://shop.oreilly.com/product/0636920036142.do)

* Page 15: Use Case of Docker not well suited for databases
  * Is it posible for me to keep the content of a mysql Database in succh a container ...
* Docker means stateless allplications ....

## Containers versus virtual machines (page 15):

* it is a threaded VM ?

## Containers are lightweight (p 16)

* A container only 12Kb... Only reference (inodes) to filesystem ?

## Limited isolation

* By default containers share CPU and memory like Unix processes ...
  * possible to constraint but require customization ...

* Difference between image_id and container_id
>  _It’s often the case that many containers share one or more common filesystem layers.
  That’s one of the more powerful design decisions in Docker, but it also means that if
  you update a shared image, you’ll need to re-create a number of containers._

## Stateless application (p 17)

* I may have a database server ?
> A good example of the kind of application that containerizes well is a web application
that keeps its state in a database.

* Do I have configuration file as code (see _settings.php_) ?
> In many cases, the process of containerizing your application means that you move
configuration state into environment variables that can be passed to your application
from the container.
.......................
it’s best to design a solution where the state can be
stored in a centralized location that could be accessed regardless of which host a con‐
tainer runs on.

## p 18 GIT and Docker

* Image filesystem configuration
* Tagging build containers

### Layered filesystem configuration

* Identification by SHA and images filsystem definition depending on existing wich may layered by be stored on your filesystem !!!

* Image tagging allows to tag each application !!!

## Building (p 20)

* p 21: testing against anoter container can be fixed with the other's container id which garantees that I have to do with the others container version of work !!!
> _expand this to a service-oriented architecture with innumerable microservices,
Docker containers can be a real lifeline to developers or QA engineers who need to
wade into the swamp of inter-microservice API calls._

## Packaging p 21 ...

* Multilayered docker image !!!

## Docker Community

* How to contibute: [Contribute to the Documentation](https://docs.docker.com/opensource/)
* __#docker-dev__ Freenode Channel
* [Google Group](https://groups.google.com/forum/#!forum/docker-dev)
* [Moby project just meant for contibution](https://blog.docker.com/2017/04/introducing-the-moby-project/)

## p 23: Atomic Hosts

>What if you could extend that core container pattern all the way down into the oper‐
ating system? Instead of relying on configuration management to try to update,
patch, and coalesce changes to your OS components, what if you could simply pull
down a new, thin OS image and reboot the server? And then if something breaks,
easily roll back to the exact image you were previously using?

* Chapter 3 will show the use of Atomic Hosts !!!!
